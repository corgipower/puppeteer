import puppeteer from 'puppeteer';

(async () => {
  const browser = await puppeteer.launch({headless: false}); // default is true
  const page = await browser.newPage();

  await page.goto('https://asca.org/events/');

  const resultsSelector = '.tribe-events-calendar-month__multiday-event-hidden-link, .tribe-events-calendar-month__calendar-event-title-link';
  await page.waitForSelector(resultsSelector);

  //sample code
  // Extract the results from the page.
//   const links = await page.evaluate(resultsSelector => {
//     return [...document.querySelectorAll(resultsSelector)].map(anchor => {
//       const title = anchor.textContent.split('|')[0].trim();
//       return `${title} - ${anchor.href}`;
//     });
//   }, resultsSelector);

  const hoverDivs = await page.$$('.tribe-events-calendar-month__multiday-event-hidden-link');

  let tt
  for (let hoverDiv of hoverDivs) {
      //hover on each element handle
      await hoverDiv.hover();
      tt = await page.waitForSelector('.tooltipster-base')
      
      const ttip = await page.evaluate((tt) => {
        return [...document.querySelectorAll('.tooltipster-base')].map(a => {
            return a.innerHTML
        })
      }, tt)
      
      console.log(ttip)
  }

//   let uniqueChars = [];
//   links.forEach((element) => {
//     if (!uniqueChars.includes(element)) {
//         uniqueChars.push(element);
//     }
// });


// console.log(uniqueChars.length)
// console.log(uniqueChars);

  // Print all the files.
  // console.log(links.join('\n'));

  await browser.close();
})();